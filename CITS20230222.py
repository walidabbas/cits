class Animal:
    def __init__(self, name):
        self.name = name

    def get_my_information(self):
        print("I am a ", self.name)


class Cat(Animal):
    def __init__(self):
        super().__init__("Cat")

    def get_my_information(self):
        print("I am a ", self.name + " " + self.name)

    def make_sound(self, voice):
        pass


class Dog(Animal):
    def __init__(self):
        super().__init__("Dog")

    def make_sound(self):
        print("Bark")


x = Cat()
print(x.name)
x.get_my_information()
y = Dog()
print(y.name)
y.get_my_information()
